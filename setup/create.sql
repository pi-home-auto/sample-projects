--- Initialize the Homeauto database

CREATE TABLE appliance (uuid TEXT, name TEXT, description TEXT, type TEXT,
       state TEXT, location TEXT, PRIMARY KEY(uuid));

CREATE TABLE configuration (module_name TEXT, description TEXT, log_level INTEGER,
       main_group TEXT,
       PRIMARY KEY(module_name));

CREATE TABLE device (uuid TEXT, name TEXT, description TEXT, location TEXT,
       grouping TEXT, type TEXT, state TEXT, addresses TEXT, parameters TEXT,
       gps TEXT, appliance_uuid TEXT, module_name TEXT, purpose TEXT, seq_order INTEGER,
       PRIMARY KEY(uuid));

CREATE TABLE device_type (name TEXT, description TEXT, handler TEXT, handler2 TEXT,
       ctlsvc_handler TEXT,
       PRIMARY KEY(name));

CREATE TABLE grouping (_id INTEGER PRIMARY KEY,
        uuid TEXT, name TEXT, description TEXT, state TEXT, parent_group TEXT);

CREATE TABLE group_item (name TEXT, grouping_id INTEGER, device_uuid TEXT);

CREATE TABLE handler (name TEXT, description TEXT, classname TEXT, parameters TEXT,
       preload INTEGER);

CREATE TABLE media_file (filename TEXT, mediatype TEXT, description TEXT,
       source_uuid TEXT, timestamp DATETIME, frame INTEGER, mimetype TEXT,
       PRIMARY KEY(filename,mediatype));

CREATE TABLE media_type (name TEXT, description TEXT, handler TEXT, parameters TEXT);

CREATE TABLE system_module (name TEXT, role TEXT, url TEXT, state TEXT,
       control_uri TEXT, control_port INTEGER);
