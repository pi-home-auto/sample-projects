
--- Populate the data used by all configurations

INSERT INTO handler(name, description, classname, preload)
  VALUES("Nop", "No op", "Nop", 1);
INSERT INTO handler(name, description, classname, preload)
  VALUES("DoorOpener", "Garage Door handler", "DoorOpener", 1);
INSERT INTO handler(name, description, classname, preload)
  VALUES("DoorSensor", "Door open/close state handler", "DoorSensor", 1);
INSERT INTO handler(name, description, classname, parameters, preload)
  VALUES("Gpio", "Gpio handler", "Gpio", "0,1,01,10", 1);
INSERT INTO handler(name, description, classname, preload)
  VALUES("KeyFob", "Remote control handler", "KeyFob", 1);
INSERT INTO handler(name, description, classname, preload)
  VALUES("MotionDetector", "Motion detector handler", "Motion", 1);
INSERT INTO handler(name, description, classname, preload)
  VALUES("Picture", "Picture handler", "Picture", 1);
INSERT INTO handler(name, description, classname, preload)
  VALUES("Servo", "Servo motor handler", "Servo", 1);
INSERT INTO handler(name, description, classname, preload)
  VALUES("Sound", "Sound handler", "Sound", 1);
INSERT INTO handler(name, description, classname, preload)
  VALUES("StepMotor", "Stepping motor handler", "StepMotor", 1);
INSERT INTO handler(name, description, classname, preload)
  VALUES("Stream", "Stream handler", "Stream", 1);
INSERT INTO handler(name, description, classname, preload)
  VALUES("KnockSensor", "Door knock sensor handler", "KnockSensor", 1);
INSERT INTO handler(name, description, classname, preload)
  VALUES("DepthSensor", "Ultra Sound Sensors", "DepthSensor", 1);

INSERT INTO device_type(name, description, handler, handler2, ctlsvc_handler)
  VALUES ("camera", "Camera and capture device", "Stream", "Picture", "");
INSERT INTO device_type(name, description, handler, ctlsvc_handler)
  VALUES ("dimmer", "Dimmer device", "Gpio", "gpio");
INSERT INTO device_type(name, description, handler, ctlsvc_handler)
  VALUES ("doorsensor", "Door sensor", "DoorSensor", "");
INSERT INTO device_type(name, description, handler, ctlsvc_handler)
  VALUES ("input", "General input", "Gpio", "gpio");
INSERT INTO device_type(name, description, handler, ctlsvc_handler)
  VALUES ("opener", "Garge Door Opener", "DoorOpener", "garage");
INSERT INTO device_type(name, description, handler, ctlsvc_handler)
  VALUES ("motion", "IR motion detector", "MotionDetector", "gpio");
INSERT INTO device_type(name, description, handler, ctlsvc_handler)
  VALUES ("remote", "Remote control opening device", "KeyFob", "");
INSERT INTO device_type(name, description, handler, ctlsvc_handler)
  VALUES ("sensor", "Sensor device", "Gpio", "gpio");
INSERT INTO device_type(name, description, handler, ctlsvc_handler)
  VALUES ("servo", "Servo device", "Servo", "servo");
INSERT INTO device_type(name, description, handler, ctlsvc_handler)
  VALUES ("stepmotor", "Step motor device", "StepMotor", "stepmotor");
INSERT INTO device_type(name, description, handler, ctlsvc_handler)
  VALUES ("button", "Button device", "Gpio", "gpio");
INSERT INTO device_type(name, description, handler, ctlsvc_handler)
  VALUES ("pwm", "Gpio pwm output device", "Gpio", "gpio");
INSERT INTO device_type(name, description, handler, ctlsvc_handler)
  VALUES ("switch", "Switch device", "Gpio", "gpio");
INSERT INTO device_type(name, description, handler, ctlsvc_handler)
  VALUES ("sound", "Sound device", "Sound", "sound");
INSERT INTO device_type(name, description, handler, ctlsvc_handler)
  VALUES ("knocksensor", "Knock sensor", "KnockSensor", "");
INSERT INTO device_type(name, description, handler, ctlsvc_handler)
  VALUES ("depthsensor", "Ultra Sound Sensor", "DepthSensor", "distance");
