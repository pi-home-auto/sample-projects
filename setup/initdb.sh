#!/bin/bash
#
# TODO Generate common-config, module-config.json files to be placed into
#      etc/ directory

USAGE="Usage:  $0 sql_script_name"
if [ $# -lt 1 ]; then
    echo $USAGE
    exit 1
fi
LOADFILE=$1
if [ ! -f "$LOADFILE" ]; then
    echo "File $LOADFILE does not exist."
    exit 2
fi
HADIR=$(dirname $LOADFILE)
HAFILE=$(basename $LOADFILE)
UNIT=${HAFILE%.sql}
DBDIR=${HADIR}
DBFILE=${DBDIR}/${UNIT}.db
SETUPDIR=$(dirname $0)
echo Loading $LOADFILE into $DBFILE

if [ -f "$DBFILE" ]; then
    mv "$DBFILE" "$DBFILE"OLD
    echo "Existing data backed up to $DBFILE"OLD
else
    mkdir -p ${DBDIR}
fi

echo "Create $UNIT db and load plan data from $LOADFILE"
sqlite3 "$DBFILE" <<EOF
.read $SETUPDIR/create.sql
.read $SETUPDIR/commonloader.sql
.read $LOADFILE
EOF

if [ -f "$DBFILE"OLD ]; then
    sqlite3 "$DBFILE"OLD ".dump media_file" | grep INSERT | sqlite3 "$DBFILE"
fi
