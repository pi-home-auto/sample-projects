#!/bin/bash
#
# Load the sql database and copy all configs and data to homeauto/etc.
#
# TODO Generate common-config, module-config.json files to be placed into
#      etc/ directory
# For now, these files are copied from the specified config directory, such
# as "simple". Since these json configs should be per host, then the configuration
# file named $hostname-config.json is used if it exists. Otherwise, the file
# $configname-config.json (i.e, simple-config.json).
#
# TODO Allow a 2nd commandline argument to specify the actual name to use.
#      This allows generating configurations for other hosts.
#

[ -z "$HA_BASE_DIR" ] && (echo "$0 Error: Must set HA_BASE_DIR environment variable."; exit 3)

function USAGE {
    cat <<EOF

Usage:  $0 [ -bhm ] config_name"
  where:
    config_name is name of the directory (i.e., simple)
    -b  Backup the current homeauto/etc directory before writing to it
    -h  Make a config based on the hostname instead of its original name
    -m  Merge media file from existing database into new database
    -?  Display this help

EOF
    exit 1
}

if [[ $# -lt 1 || "$1" = "-?" ]]; then
    USAGE
fi

BackupEtc=no
MergeExistingMedia=no
UseHostName=no
while getopts "bhm" opt; do
    case $opt in
	b) BackupEtc=yes
	   ;;
	h) UseHostName=yes
	   ;;
	m) MergeExistingMedia=yes
	   ;;
	*) USAGE
	   ;;
    esac
done
shift $((OPTIND - 1))
[ $# -eq 1 ] || USAGE

SetupDir=$(dirname $0)
BaseDir=$SetupDir/..
ConfigName="$1"
ConfigDir="$BaseDir/$ConfigName"
UseName=$ConfigName
[ $UseHostName = yes ] && UseName=$(hostname)

echo
echo Make $ConfigName from $ConfigDir backup=$BackupEtc useName=$UseName
echo -n 'Continue? (y/n) '
read YN
if [ x"$YN" != xy ]; then
    echo Exiting...
    exit 5
fi

rm -rf $SetupDir/etc
mkdir $SetupDir/etc
if [ -f $ConfigDir/$UseName-config.json ]; then
    echo "Using hostname-based json config file"
    cp $ConfigDir/$UseName-config.json $SetupDir/etc/$UseName-config.json
else
    echo "Using confignamet-based json config file"
    cp $ConfigDir/$ConfigName-config.json $SetupDir/etc/$UseName-config.json
fi
cp $SetupDir/commonconfig.json $SetupDir/etc/
cp $ConfigDir/*.rbs $SetupDir/etc/

echo "ConfigDir $ConfigDir  ConfigName $ConfigName"

if [ $MergeExistingMedia == yes ]; then
    cp $HA_BASE_DIR/data/$HA_DBFILE $ConfigDir/$ConfigName.db
else
    rm -f $ConfigDir/$ConfigName.db
fi
$SetupDir/initdb.sh $ConfigDir/$ConfigName.sql

DestDir="$HA_BASE_DIR/etc"
BackupDir="$HA_BASE_DIR/Oetc"
if [ $BackupEtc = "yes" ]; then
    if [ -d "$BackupDir" ]; then
	echo "$0 Error: $BackupDir already exists -- not overwriting."
	exit 5
    fi
    cp -rp $DestDir $BackupDir
fi
cp -rp $SetupDir/etc "$HA_BASE_DIR"
mkdir -p $HA_BASE_DIR/data $HA_BASE_DIR/setup
cp $ConfigDir/$ConfigName.db $HA_BASE_DIR/data
cp $ConfigDir/$ConfigName.sql $HA_BASE_DIR/setup
echo "Configuration $ConfigName loaded to dir $HA_BASE_DIR as $UseName on $(date)." > $HA_BASE_DIR/setup/config-info.txt

exit 0
