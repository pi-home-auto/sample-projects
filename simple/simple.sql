
--- Populate the database for a simple confiuration
--- This config has 1 LED and 1 Push button.

--- Details that are specific to this module
INSERT INTO configuration(module_name, description, log_level, main_group)
  VALUES ("simple", "Simple Home Automation Example", 5, "Simple");

INSERT INTO system_module(name, role, url, state, control_uri, control_port)
  VALUES ("simple", "base", "http://simple:8888", "(unknown)", "localhost", 1800);

INSERT INTO grouping(uuid, name, description, state)
  VALUES ("123-456-78", "Simple", "Group of devices in simple unit", "enabled");

--- Status LED
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, module_name)
  VALUES ("LED-STATUS-1", "Status LED", "Base module", "Simple", "switch", "enabled", "18", "simple");

--- LED on gpio 5
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, module_name)
  VALUES ("LED-GD-0123-1", "LED", "Base module", "Simple", "switch", "enabled", "5", "simple");

--- Push button on gpio 4
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, purpose, module_name)
  VALUES ("SW-GD-8123-01", "Push Button", "Base module", "Simple", "switch", "enabled", "4", "input", "simple");
