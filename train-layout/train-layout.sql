
--- Populate the demo/prototype data.

INSERT INTO configuration(module_name, description, log_level, main_group)
  VALUES ("switchback", "Train Switchback Mountain", 5, "Train");

INSERT INTO configuration(module_name, description, log_level, main_group)
  VALUES ("tunnel", "Train Tunnel Sensing, Lighting and Control", 5, "Train");

INSERT INTO system_module(name, role, url, state, control_uri, control_port)
  VALUES ("switchback", "base", "http://switchback:8888", "(unknown)", "localhost", 1800);

INSERT INTO system_module(name, role, url, state, control_uri, control_port)
  VALUES ("tunnel", "sub", "http://tunnel:8888", "(unknown)", "localhost", 1800);

--- Devices for switchback module
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, purpose, seq_order, module_name)
  VALUES ("S-LIGHT-STREET-B", "Switchback street lights", "Base module", "Train", "switch", "enabled", "7", "light", 8, "switchback");
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, purpose, seq_order, module_name)
  VALUES ("S-LIGHT-STATION-1", "Train station lights", "Base module", "Train", "switch", "enabled", "21", "light", 1, "switchback");
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, purpose, seq_order, module_name)
  VALUES ("S-LIGHT-BRIDGE-RED-1", "Bridge Lights Red", "Base module", "Train", "switch", "enabled", "20", "light", 2, "switchback");
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, purpose, seq_order, module_name)
  VALUES ("S-LIGHT-BRIDGE-GREEN-2", "Bridge lights Green", "Base module", "Train", "switch", "enabled", "16", "light", 3, "switchback");
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, purpose, seq_order, module_name)
  VALUES ("S-LIGHT-LMILL-100", "Lumbermill main lighting", "Base module", "Train", "switch", "enabled", "12", "light", 4, "switchback");
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, purpose, seq_order, module_name)
  VALUES ("S-LED-STATUS-1", "Status LED", "Base module", "Train", "pwm", "enabled", "18", "light", 0, "switchback");
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, purpose, seq_order, module_name)
  VALUES ("S-LIGHT-WATER-TOWER", "Water tower lights", "Base module", "Train", "switch", "enabled", "26", "light", 5, "switchback");
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, purpose, seq_order, module_name)
  VALUES ("S-LIGHT-COALING-TOWER", "Coaling tower lights", "Base module", "Train", "switch", "enabled", "19", "light", 6, "switchback");
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, purpose, seq_order, module_name)
  VALUES ("S-LIGHT-COALING-DOCK", "Coaling tower garage", "Base module", "Train", "pwm", "enabled", "13", "light", 7, "switchback");

--- Stepping motor (fake entry just for testing)
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, purpose, module_name)
  VALUES ("S-SM-TEST-1234-1", "Test Step Motor", "Base module", "Train", "stepmotor", "enabled", "16 20 21 26", "motor", "switchback");

--- minipeer devices on switchback
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, purpose, seq_order, module_name)
  VALUES ("MP-LOGOPS-INPUT-0", "Switchback minipeer input 0", "Base module", "Train", "input", "enabled", "0 minipeer.logops.i0", "button", 99, "switchback");
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, purpose, seq_order, module_name)
  VALUES ("MP-LOGOPS-INPUT-1", "Switchback minipeer input 1", "Base module", "Train", "input", "enabled", "1 minipeer.logops.i1", "turnout", 99, "switchback");
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, purpose, seq_order, module_name)
  VALUES ("MP-LOGOPS-INPUT-2", "Switchback minipeer input 2", "Base module", "Train", "input", "enabled", "2 minipeer.logops.i2", "turnout", 99, "switchback");
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, purpose, seq_order, module_name)
  VALUES ("MP-LOGOPS-OUTPUT-0", "Switchback minipeer output 0", "Base module", "Train", "pwm", "enabled", "0 minipeer.logops.o0", "light", 99, "switchback");
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, purpose, seq_order, module_name)
  VALUES ("MP-LOGOPS-OUTPUT-1", "Switchback minipeer output 1", "Base module", "Train", "pwm", "enabled", "1 minipeer.logops.o1", "light", 99, "switchback");
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, purpose, seq_order, module_name)
  VALUES ("MP-LOGOPS-OUTPUT-2", "Switchback minipeer output 2", "Base module", "Train", "pwm", "enabled", "2 minipeer.logops.o2", "light", 99, "switchback");
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, purpose, seq_order, module_name)
  VALUES ("MP-LOGOPS-MOTOR-0", "Switchback minipeer step motor 0", "Base module", "Train", "stepmotor", "enabled", "0 minipeer.logops.s0", "light", 99, "switchback");

--- Devices for tunnel module
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, seq_order, module_name)
  VALUES ("T-LED-GD-0123-1", "Track 1 Green", "Sub module", "Train", "switch", "enabled", "23", 1, "tunnel");
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, seq_order, module_name)
  VALUES ("T-LED-GD-0123-2", "Track 1 Red ", "Sub module", "Train", "switch", "enabled", "24", 2, "tunnel");
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, seq_order, module_name)
  VALUES ("T-LED-GD-0123-3", "Track 2 Green", "Sub module", "Train", "switch", "enabled", "27", 3, "tunnel");
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, seq_order, module_name)
  VALUES ("T-LED-GD-0123-4", "Track 2 Red", "Sub module", "Train", "switch", "enabled", "22", 4, "tunnel");
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, seq_order, module_name)
  VALUES ("T-LED-STATUS-1", "Status LED", "Sub module", "Train", "switch", "enabled", "18", 0, "tunnel");

--- Relay 1-4
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, purpose, seq_order, module_name)
  VALUES ("T-CB-NW-1", "Crossbucks", "Sub module", "Train", "switch", "enabled", "17", "light", 5, "tunnel");
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, purpose, seq_order, module_name)
  VALUES ("STREET-WWSHOP-1", "Street lights", "Sub module", "Train", "switch", "enabled", "5", "light", 14, "tunnel");
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, purpose, seq_order, module_name)
  VALUES ("WWSHOP-EXT-LIGHT-1", "Workworking shop interior lighting", "Sub module", "Train", "switch", "enabled", "6", "light", 15, "tunnel");
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, purpose, seq_order, module_name)
  VALUES ("LUMBER-BLD-1", "Lumberyard Interior", "Sub module", "Train", "switch", "enabled", "11", "light", 16, "tunnel");

INSERT INTO device(uuid, name, location, grouping, type, state, addresses, purpose, seq_order, module_name)
  VALUES ("WWSHOP-INT-LIGHT-2", "Workworking shop exterior lighting", "Sub module", "Train", "switch", "enabled", "19", "light", 13, "tunnel");

--- Log cabin
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, purpose, seq_order, module_name)
  VALUES ("T-LED-CABIN-PORCH-1", "Cabin Porch", "Sub module", "Train", "pwm", "enabled", "12", "light", 6, "tunnel");
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, purpose, seq_order, module_name)
  VALUES ("T-LED-CABIN-INTERIOR-1", "Cabin Interior", "Sub module", "Train", "pwm", "enabled", "13", "light", 7, "tunnel");

--- Servo
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, module_name)
  VALUES ("T-CB1-SERVO0-1", "Crossing gate 1", "Sub module", "Train", "servo", "enabled", "16", "tunnel");

--- Sound
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, module_name)
  VALUES ("S-AU0-SOUND-00", "Sound device", "Sub module", "Train", "sound", "enabled", "1", "switchback");

INSERT INTO device(uuid, name, location, grouping, type, state, addresses, module_name)
  VALUES ("T-AU1-SOUND0-5", "Sound device", "Sub module", "Train", "sound", "enabled", "1", "tunnel");

--- Stepping motor
--- INSERT INTO device(uuid, name, location, grouping, type, state, addresses, module_name)
---  VALUES ("CB1-STPMOTOR-1", "Crossing gate 1", "Sub module", "Train", "stepmotor", "enabled", "16 20 21 26", "tunnel");

INSERT INTO device(uuid, name, location, grouping, type, state, addresses, purpose, seq_order, module_name)
  VALUES ("T-CAR-AA-0123-1", "Station Wagon", "Sub module", "Train", "switch", "enabled", "10", "light", 12, "tunnel");

INSERT INTO device(uuid, name, location, grouping, type, state, addresses, purpose, seq_order, module_name)
  VALUES ("T-TOUT-GD-8123-03", "Turnout T2", "Sub module", "Train", "input", "enabled", "25", "turnout", 3, "tunnel");

--- Devices for motion detector module
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, seq_order, module_name)
  VALUES ("S-MD0-1001-1", "Motion Detector Room", "Sub module", "Train", "motiondetector", "enabled",
  "14", 1, "switchback");

INSERT INTO device(uuid, name, location, grouping, type, state, addresses, seq_order, module_name)
  VALUES ("T-MD-1001-1", "Motion Detector South", "Sub module", "Train", "motiondetector", "enabled",
  "4", 1, "tunnel");

INSERT INTO device(uuid, name, location, grouping, type, state, addresses, seq_order, module_name)
  VALUES ("T-MD-1001-2", "Motion Detector East", "Sub module", "Train", "motiondetector", "enabled",
  "14", 2, "tunnel");

--- Devices from camera modules
INSERT INTO device(uuid, name, location, grouping, type, state, addresses, gps,
                   parameters, module_name)
  VALUES ("T-CAM-123-1", "Tunnel Camera", "Train Room", "Train", "camera", "enabled",
          "0", "37.3883852,-121.9367605,16z",
          "http://@HOST@:8088/stream_simple.html,rtsp://@HOST@:8000/pi.sdp", "tunnel");

INSERT INTO device(uuid, name, location, grouping, type, state, addresses, gps,
                   parameters, module_name)
  VALUES ("S-CAM-MTN123-1", "Switchback Mountain Camera", "Train Room", "Train", "camera", "enabled",
          "0", "37.3883852,-121.9367605,16z",
          "http://@HOST@:8088/stream_simple.html,rtsp://@HOST@:8000/pi.sdp", "switchback");

--- others
INSERT INTO grouping(uuid, name, description, state)
  VALUES ("123-456-78", "Train", "Group of devices in tunnel unit", "enabled");

--- Some predefined sounds

INSERT INTO media_file (filename, mediatype, description, source_uuid, timestamp)
  VALUES ("cass-nr11.mp3", "sound", "Cass Scenic Shay Nr. 11", "(download)", "2022-06-09 16:21");
INSERT INTO media_file (filename, mediatype, description, source_uuid, timestamp)
  VALUES ("duck1.mp3", "sound", "Duck quack", "(download)", "2022-01-02 10:01");
INSERT INTO media_file (filename, mediatype, description, source_uuid, timestamp)
  VALUES ("Duck-QuickSounds.mp3", "sound", "Quick duck quack", "(download)", "2022-01-02 10:01");
INSERT INTO media_file (filename, mediatype, description, source_uuid, timestamp)
  VALUES ("greenbrier-river.mp3", "sound", "Greenbrier river", "(download)", "2022-01-02 10:01");
INSERT INTO media_file (filename, mediatype, description, source_uuid, timestamp)
  VALUES ("mixkit-night-crickets-near-the-swamp-1782.wav", "sound", "Night crickets near swamp", "(download)", "2022-01-02 10:01");
INSERT INTO media_file (filename, mediatype, description, source_uuid, timestamp)
  VALUES ("mixkit-summer-crickets-loop-1788.wav", "sound", "Summer crickets", "(download)", "2022-01-02 10:01");
INSERT INTO media_file (filename, mediatype, description, source_uuid, timestamp)
  VALUES ("mixkit-summer-night-in-the-forest-1227.wav", "sound", "Summer night in the forest", "(download)", "2022-01-02 10:01");
INSERT INTO media_file (filename, mediatype, description, source_uuid, timestamp)
  VALUES ("Night-cricket-sound-effect.mp3", "sound", "Night crickets", "(download)", "2022-01-02 10:01");
INSERT INTO media_file (filename, mediatype, description, source_uuid, timestamp)
  VALUES ("salamisound-8793613-steam-locomotive-whistle.mp3", "sound", "Steam train whistle (salami)", "(download)", "2022-01-02 10:01");
INSERT INTO media_file (filename, mediatype, description, source_uuid, timestamp)
  VALUES ("steam-train-daniel_simon.mp3", "sound", "Steam train", "(download)", "2022-01-02 10:01");
INSERT INTO media_file (filename, mediatype, description, source_uuid, timestamp)
  VALUES ("steam-train-whistle-daniel_simon.mp3", "sound", "Steam train whistle", "(download)", "2022-01-02 10:01");
INSERT INTO media_file (filename, mediatype, description, source_uuid, timestamp)
  VALUES ("train-crossing-bell-01.wav", "sound", "Train crossing bell", "(download)", "2022-01-02 10:01");
