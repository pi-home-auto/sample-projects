# Sample projects

## Name
Sample projects for Pi Home Automation setups

## Description
The programs in the Pi Home Automation group work by reading in configuration file. However, none of these configuration files have been included in the projects.  Thus this project holds a collection of various configurations. There are also scripts to load a sqlite database and configure other info.

If you are new to this project, then follow the Installation section below. Then to get started, setup and run the simple project.

## Installation
You will need a Raspberry Pi (models 2/3/4 work) and connect the various LEDs, servos, relays and other devices you will be controling.
Then the software projects within the Pi Home Automation group need to be installed: controlservice and REST/Web server. These projects make sure of other libraries as shown below:

#### Controlservice dependencies
* [sockstr](https://github.com/lawarner/sockstr) - C++ socket/stream library
* pigpio - Gpio interface library for Raspberry Pi.
* [googletest](https://github.com/google/googletest) (optional) Use for unit tests
* openssl (optional) - if using https or other encryption

#### REST/Web server dependencies
* node.js - For the REST and web front-end
* sqlite - to load and modify the automation plan database

### Step-by-step guide
First you need to decide what user to run as. The instructions below refer to user 'pi' but I always create my own user ID on the Pi and use that.

> Note that in the instructions below when a step starts with *$* that indicates that you type the rest of that line at the shell prompt. Thus the dollar sign represents the shell prompt and you should not type it.

Set up user:

    login: pi
    password:
    $ cd
    $ mkdir -p homeauto src

Add the following lines near the end of your .profile or .bashrc. Then logout and login again for the changes to take effect.

    export HA_BASE_DIR=/home/pi/homeauto
    export PATH="$PATH:$HA_BASE_DIR/bin"
    echo "Home automation directory set as $HA_BASE_DIR"

Install prerequisites:

    $ sudo apt install libssl-dev nodejs sqlite
    $ cd src
    $ git clone https://github.com/lawarner/sockstr.git
    $ cd sockstr/include/sockstr
    $ ln -s sstypes-linux.h sstypes.h
    $ cd ../..
    $ make
    $ make install

    $ cd $HOME/src
    $ git clone https://github.com/joan2937/pigpio.git
    $ cd pigpio
    $ make
    $ sudo make install

    $ cd $HOME/src
    $ git clone https://github.com/google/googletest.git
    $ cd googletest
    $ mkdir build
    $ cd build
    $ cmake ..
    $ make
    $ sudo make install

    $ cd $HOME/src
    $ git clone https://gitlab.com/pi-home-auto/controlservice.git
    $ cd controlservice
    $ make
    $ make install


## Usage
To run this solution, you must first install a configuration and then start both controlservice and the node.js front-end.

To setup the sample 'simple' configuration, do the following:

    $ cd $HOME/src
    $ git clone https://gitlab.com/pi-home-auto/sample-projects.git
    $ cd sample-projects
    $ setup/makeconfig.sh -b -h simple

To start controlservice, run:

    $ controlservice -b

You should see the logging of the controlservice on your terminal. Scan this to make sure there are no errors. If all goes well, the status LED should blink on and off every second. The other LED should remain on until you push the button.

## Support
TBD

## Notes
Use '.tables' to view table names and '.schema <table>' to view columns of a table (within sqlite3 CLI).

## Roadmap / TO DO

* For the simple configuration all documentation and a Pi wiring diagram. There is basically just 2 LEDs, 3 resistors and a push button.

## Contributing
TBD

## Authors and acknowledgment
Just me for now.

## License
These samples are for educational purposes and are not licensed. Thus they are in the public domain.

## Project status
Still getting the first few projects working. Need to finish the README step-by-step instructions.

